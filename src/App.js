import React, { Component } from 'react';

import './App.css';
import Header from './components/Header/Header';
import AboutMe from './components/AboutMe/AboutMe';
import WorkExperience from './components/WorkExperience/WorkExperience';
import Skills from './components/Skills/Skills';
import Education from './components/Education/Education';
import Languages from './components/Languages/Languages';
import Personality from './components/Personality/Personality';
import OnTopButton from './components/UI/OnTopButton/OnTopButton';
import Contact from './components/Contact/Contact';

class App extends Component {
  state = {
    position: null,
    showNav: false
  }

  componentDidMount(){
    window.addEventListener('scroll', this.scrollHandle)
  }

  scrollHandle = () => {
    const position = window.pageYOffset
    this.setState({position: position});
  }

  scrollOnTopHandle = () => {
    window.scrollTo({top: 0, left: 0, behavior: 'smooth'});
  }

  handleHamBtn = () => {
    this.setState(prevState => ({
      showNav: !prevState.showNav
    }));
  }


  render() {
    let onTopButton = null;
    if(this.state.position>0){
      onTopButton = <OnTopButton onTop={this.scrollOnTopHandle} />
    }
    return (
      <React.Fragment>
        <Header handleHamBtn={this.handleHamBtn} showNavbar={this.state.showNav}/>
        <AboutMe />
        <WorkExperience scroll={this.state.position} />
        <Skills scroll={this.state.position} />
        <Education />
        <Languages />
        <Personality scroll={this.state.position} />
        <Contact />
        {onTopButton}
      </React.Fragment>
    );
  }
}

export default App;
