import React from 'react';

import classes from './Personality.module.scss';

const Personality = (props) => {
    const assignedClasses1 = [classes.Item];
    const assignedClasses2 = [classes.Item];
    const assignedClassesRight1 = [classes.Item];
    const assignedClassesRight2 = [classes.Item];

    if(props.scroll>3200) {
        assignedClasses1.push(classes.Item1)
        assignedClasses2.push(classes.Item2)
        assignedClassesRight1.push(classes.ItemRight1)
        assignedClassesRight2.push(classes.ItemRight2)
    }
    return (
        <div className={classes.Personality} id="personality">
            <div className={classes.PersonalityContainer}>
            <h2>Personaliy</h2>
                <div className={classes.PersonalityList}>
                    <ul>
                        <li className={assignedClasses1.join(' ')}>Communicative</li>
                        <li className={assignedClasses2.join(' ')}>Dedicated</li>
                    </ul>
                </div>
                <div className={classes.PersonalityList}>
                    <ul>
                        <li className={assignedClassesRight1.join(' ')}>Persistent</li>
                        <li className={assignedClassesRight2.join(' ')}>Organized</li>
                    </ul>
                </div>
            </div>
            <div className={classes.BottomLine}></div>
        </div>
    )
}

export default Personality