import React from 'react';

import classes from './PersonalData.module.scss';

const PersonalData = () => {
    return (
        <div className={classes.PersonalData}>
            <div className={`${classes.Hexagon} ${classes.Image}`}></div>
            <div className={classes.PersonalInfo}>
                <ul>
                    <li><span>Name:</span> Nikola Brkusanin</li>
                    <li><span>Date of Birth:</span> 03 Oct 1983</li>
                    <li><span>Address:</span> 15 Pecinska Street, Kraljevo</li>
                    <li><span>Nationality</span> Serbian</li>
                    <li><span>Phone:</span> +38160 411 4880</li>
                    <li><span>Email:</span> nbrkusanin@gmail.com</li>
                </ul>
            </div>
        </div>
    )
}

export default PersonalData