import React from 'react';

import classes from './AboutMe.module.scss';
import PersonalData from './PersonalData/PersonalData';

const AboutMe = () => {
    return (
        <div className={classes.AboutMe} id="AboutMe">
            <div className={classes.AboutMeContainer}>
                <h2>About Me</h2>
                <div className={classes.TextAboutMe}>
                    <p><span>I</span> enjoy to write code. Every day I try to progress to new knowledge and do not shy away from learning and work, because I am an open-minded person willing to both stand- alone and for teamwork.
                                    I consider myself a dedicated and persistent person, with clear objectives that I want to achieve in the professional field.</p>
                </div>
                <PersonalData />
            </div>
            <div className={classes.BottomLine}></div>
        </div>
    )
}

export default AboutMe