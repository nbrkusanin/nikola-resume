import React from 'react';

import classes from './WorkExperience.module.scss';

const WorkExperience = (props) => {
    
    const assignedClasses1 = [classes.TimelinePanel];
    const assignedClasses2 = [classes.TimelinePanelInverted];
    const assignedClasses3 = [classes.TimelinePanel];

    if(props.scroll >= 792){
        assignedClasses1.push(classes.TimelinePanelAnimateLeft1)
    }
    if(props.scroll >= 992){
        assignedClasses2.push(classes.TimelinePanelAnimateRight1)
    }
    if(props.scroll >= 1092){
        assignedClasses3.push(classes.TimelinePanelAnimateLeft2)
    }
    
    return(
        <div className={classes.WorkExperience} id="WorkExperience">
            <div className={classes.WorkExperienceContainer}>
                <h2>Work Experience</h2>
                <div className={classes.TimelineContainer}>
                    <ul className={classes.Timeline}>
                        <li>
                            <div className={classes.PostedDate}>
                                <span className={classes.Year}>2019-Present</span>
                            </div>
                            <div className={assignedClasses1.join(' ')}>
                                <div className={classes.TimelinePanelContent}>
                                    <h3>Fronend Developer at Quantox Technology</h3>
                                    <p>Building responsive and user friendly website from the ground up - from concept to its final looks</p>
                                </div>
                            </div> 
                        </li>
                        <li className={classes.Inverted}>
                            <div className={classes.PostedDate}>
                                <span className={classes.Year}>2014-2019</span>
                            </div>
                            <div className={assignedClasses2.join(' ')}>
                                <div className={classes.TimelinePanelContent}>
                                    <h3>Sales Agent at Nelt Group</h3>
                                    <p>Negotiating with customers, closing the sale, drafting a reports etc.</p>
                                </div>
                            </div> 
                        </li>
                        <li>
                            <div className={classes.PostedDate}>
                                <span className={classes.Year}>2008-2014</span>
                            </div>
                            <div className={assignedClasses3.join(' ')}>
                                <div className={classes.TimelinePanelContent}>
                                    <h3>Accountant at Mataruska Spa - SE</h3>
                                    <p>Bookkeeping (treasury operations, financial, material accounting, e-banking, making financial reports, etc.</p>
                                </div>
                            </div> 
                        </li>

                    </ul>
                </div>
            </div>
            <div className={classes.BottomLine}></div>
        </div>
    )
}

export default WorkExperience