import React, { Component } from 'react';

import classes from './Contact.module.scss';
import Modal from '../UI/Modal/Modal';
import ContactForm from './ContactForm/ContactForm';

class Contact extends Component {

    state = {
        showModal: false
    }

    toggleModal = () => {
        this.setState({showModal: !this.state.showModal})
    }

    render() {

        return(
            <div className={classes.Contact} id="contact">
                <div className={classes.ContactContainer}>
                    <h2>Contact</h2>
                    <div className={classes.ContactContent}>
                        <div className={classes.ContactButtonContainer}>
                            <button className={classes.ContactButton} onClick={this.toggleModal}>Send me a message</button>
                            {this.state.showModal ? (
                                <Modal show = {this.state.showModal} toggle={this.toggleModal}>
                                    <ContactForm closeModal={this.toggleModal}/>
                                </Modal>
                            ) : null}
                        </div>
                        <div className={classes.GoogleMap}>
                            <div className={classes.ContactData}>
                                <div className={classes.ContactDataContent}>
                                    <i className="fa fa-map-marker address"></i>
                                    <address>
                                        <strong>Address/Street</strong><br/>
                                        15 Pecinska Street<br/>
                                        Kraljevo, Serbia
                                    </address>
                                </div>
                                <div className={classes.ContactDataContent}>
                                    <i className="fa fa-mobile phone"></i>
                                    <strong>Phone Number</strong><br/>
                                    +38160 411 4880
                                </div>
                            </div>
                            <div className={classes.GoogleIframe}>
                                <div style={{width: "100%"}}>
                                    <iframe title="googleMap"  src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;q=15.%20Pecinska%2C%20Mataruska%20Banja+(Nikola%20Brkusanin)&amp;ie=UTF8&amp;t=&amp;z=17&amp;iwloc=B&amp;output=embed" >
                                        <a href="https://www.maps.ie/coordinates.html">find my location</a>
                                    </iframe>
                                </div><br />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Contact