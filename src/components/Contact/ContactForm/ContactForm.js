import React, { Component } from 'react';

import classes from './ContactForm.module.scss';

 class ContactForm extends Component {
  
    state = {
      status: "",
      orderForm: {
        name:{
            validation:{
                required: true
            },
            valid: false,
            touched: false,
            value: ''
        },
        mail:{
            validation:{
                required: true, 
                regEx: /^\S+@\S+\.\S+$/
            },
            valid: false,
            touched: false,
            value: ''
        },
        message:{
            validation:{
                required: true
            },
            valid: false,
            touched: false,
            value: ''
        }
      },
      formIsValid: false
    };
  

  checkValidity(value, rules) {
    let isValid = true;

    if(rules.required) {
        isValid = value.trim() !== '' && isValid;
    }

    if(rules.regEx) {
        isValid =rules.regEx.test(value) && isValid;
    }

    return isValid;
}

inputChangeHandler = (event, inputIndentifier) => {
  const updatedOrderForm = {
      ...this.state.orderForm
  };
  const updatedFormElement = {
      ...updatedOrderForm[inputIndentifier]
  };
  updatedFormElement.value = event.target.value;
  updatedFormElement.valid = this.checkValidity(updatedFormElement.value, updatedFormElement.validation);
  updatedFormElement.touched = true;
  updatedOrderForm[inputIndentifier] = updatedFormElement;
  
  let formIsValid = true;

  for(let inputIndentifier in updatedOrderForm){
      formIsValid = updatedOrderForm[inputIndentifier].valid && formIsValid;
  }
  
  this.setState({orderForm : updatedOrderForm, formIsValid : formIsValid});
}

  render() {
    console.log(this.state)
    const { status } = this.state;
    const { name } = this.state.orderForm;
    const { mail } = this.state.orderForm;
    const { message } = this.state.orderForm;
    const inputClassesName = [classes.Input];
    const inputClassesMail = [classes.Input];
    const textareaClasses = [classes.Textarea];

    if(name.invalid && name.shouldValidate && name.touched){
      inputClassesName.push(classes.Invalid)
    }
    if(mail.invalid && mail.shouldValidate && mail.touched){
      inputClassesMail.push(classes.Invalid)
    }
    if(message.invalid && message.shouldValidate && message.touched){
      textareaClasses.push(classes.Invalid)
    }
    
    return (
      <form
        className={classes.ContactForm}
        onSubmit={this.submitForm}
        action="https://formspree.io/xyywwvpp"
        method="POST"
      >
        <div className={classes.InputContainer}>
            <label>Your Name:</label>
            <input 
              type="text" 
              name="name" 
              className={inputClassesName.join(' ')}
              key="name" 
              value={name.value}
              onChange={(event) => this.inputChangeHandler(event, 'name')}/>
        </div>  
        <div className={classes.InputContainer}>
            <label>Your Email:</label>
            <input 
              type="email" 
              name="email" 
              className={inputClassesMail.join(' ')}
              key="mail" 
              value={mail.value}
              onChange={(event) => this.inputChangeHandler(event, 'mail')}/>/>
        </div>
        <div className={classes.InputContainer}>
            <label>Message:</label>
            <textarea 
              type="textarea" 
              name="message" 
              className={textareaClasses.join(' ')}
              key="message" 
              value={message.value}
              onChange={(event) => this.inputChangeHandler(event, 'message')}/>
        </div>
        <div className={classes.ButtonContainer}>
            {status === "SUCCESS" ? <p className={classes.MessageSuccess}>Thanks!</p> : <button className={classes.SubmitBtn} disabled={!this.state.formIsValid}>Submit</button>}
            {status === "ERROR" && <p className={classes.MessageError}>Ooops! There was an error.</p>}
        </div>
      </form>
    );
  }

  submitForm = (event) => {
    event.preventDefault();
    const form = event.target;
    console.log(form);
    const data = new FormData(form);
    const xhr = new XMLHttpRequest();
    xhr.open(form.method, form.action);
    xhr.setRequestHeader("Accept", "application/json");
    xhr.onreadystatechange = () => {
      if (xhr.readyState !== XMLHttpRequest.DONE) return;
      if (xhr.status === 200) {
        form.reset();
        this.setState({ status: "SUCCESS" });
      } else {
        this.setState({ status: "ERROR" });
      }
    };
    xhr.send(data);
  }
}

export default ContactForm