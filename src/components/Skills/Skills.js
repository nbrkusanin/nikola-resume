import React from 'react';

import classes from './Skills.module.scss';

const Skills = (props) => {
    const assignedClasses1 = [classes.Item];
    const assignedClasses2 = [classes.Item];
    const assignedClasses3 = [classes.Item];
    const assignedClasses4 = [classes.Item];
    const assignedClasses5 = [classes.Item];
    const assignedClasses6 = [classes.Item];
    const assignedClassesRight1 = [classes.Item];
    const assignedClassesRight2 = [classes.Item];
    const assignedClassesRight3 = [classes.Item];
    const assignedClassesRight4 = [classes.Item];
    const assignedClassesRight5 = [classes.Item];
    const assignedClassesRight6 = [classes.Item];

    if(props.scroll >= 1500){

        assignedClasses1.push(classes.Item1)
        assignedClasses2.push(classes.Item2)
        assignedClasses3.push(classes.Item3)
        assignedClasses4.push(classes.Item4)
        assignedClasses5.push(classes.Item5)
        assignedClasses6.push(classes.Item6)
        assignedClassesRight1.push(classes.ItemRight1)
        assignedClassesRight2.push(classes.ItemRight2)
        assignedClassesRight3.push(classes.ItemRight3)
        assignedClassesRight4.push(classes.ItemRight4)
        assignedClassesRight5.push(classes.ItemRight5)
        assignedClassesRight6.push(classes.ItemRight6)
    }

    return(
        <div className={classes.Skills} id='skills'>
            <div className={classes.SkillsContainer}>
                <h2>Skills</h2>
                <div className={classes.SkillList}>
                    <ul>
                        <li className={assignedClasses1.join(' ')}>HTML, HTML5</li>
                        <li className={assignedClasses2.join(' ')}>CSS, CSS3</li>
                        <li className={assignedClasses3.join(' ')}>SASS, LESS</li>
                        <li className={assignedClasses4.join(' ')}>Bootstrap</li>
                        <li className={assignedClasses5.join(' ')}>JavaScript</li>
                        <li className={assignedClasses6.join(' ')}>jQuery</li>
                    </ul>
                </div>
                <div className={classes.SkillList}>
                    <ul>
                        <li className={assignedClassesRight1.join(' ')}>React, Redux</li>
                        <li className={assignedClassesRight2.join(' ')}>Vue, Vuex</li>
                        <li className={assignedClassesRight3.join(' ')}>Git</li>
                        <li className={assignedClassesRight4.join(' ')}>NPM</li>
                        <li className={assignedClassesRight5.join(' ')}>SEO</li>
                        <li className={assignedClassesRight6.join(' ')}>Laravel Blade</li>
                    </ul>
                </div>
            </div>
            <div className={classes.BottomLine}></div>
        </div>
    )
}

export default Skills