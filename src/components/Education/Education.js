import React from 'react';

import classes from './Education.module.scss';

const Education = () => {
    return (
        <div className={classes.Education} id="education">
            <div className={classes.EducationContainer}>
                <h2>Education</h2>
                <div className={classes.EducationListCont}>
                    <ul className={classes.EducationList}>
                        <li className={`${classes.ListItem} ${classes.Diff}`}>
                            <i className="fa fa-graduation-cap fa-3x"></i>
                            <div className={classes.ListItemData}>
                                <h3>School of Economics - Kraljevo</h3>
                                <p>Economist</p>
                            </div>
                            <i className="fa fa-graduation-cap fa-3x"></i>
                        </li>
                        <li className={classes.ListItem}>
                            <i className="fa fa-graduation-cap fa-3x"></i>
                            <div className={classes.ListItemData}>
                                <h3>College of Economics Pec-Leposavic</h3>
                                <p>Finance manager</p>
                            </div>
                            <i className="fa fa-graduation-cap fa-3x"></i>
                        </li>
                        <li className={`${classes.ListItem} ${classes.Diff}`}>
                            <i className="fa fa-graduation-cap fa-3x"></i>
                            <div className={classes.ListItemData}>
                                <h3>Faculty of Technical Sciences Cacak - Web development course</h3>
                                <p>Frontend developer</p>
                            </div>
                            <i className="fa fa-graduation-cap fa-3x"></i>
                        </li>
                    </ul>
                </div>
            </div>
            <div className={classes.BottomLine}></div>
        </div>
    )
}

export default Education