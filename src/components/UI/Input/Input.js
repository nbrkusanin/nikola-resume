import React from 'react';
import classes from '*.module.css';

const Input = (props) => {
    inputElement = null;

    switch (props.elementType) {
        case ('input'): 
            inputElement = <input 
                className={classes.InputElement}
                value={props.value}
                onChange={props.changed}/>
            break;
        case ('textarea'):
            inputElement = <textarea
                className={classes.Textarea}
                value={props.value}
                onChange={props.changed} />
            break;
        default:
            inputElement = <input 
            className={classes.InputElement}
            value={props.value}
            onChange={props.changed} />
    }
    return (
        <div className={classes.Input}>
            <label className={classes.Label}>{props.label}</label>
            {inputElement}
        </div>
    )
}

export default Input