import React, {Component} from 'react'
import { createPortal } from 'react-dom';

import Backdrop from '../Backdrop/Backdrop';
import classes from './Modal.module.scss';

const modalRoot = document.getElementById('modal');

class Modal extends Component {

    constructor(props){
        super(props);
        this.element = document.createElement('div');
    }

    componentDidMount(){
        modalRoot.appendChild(this.element);
    }

    componentWillUnmount(){
        modalRoot.removeChild(this.element);
    }

    render(){
        return createPortal (
            <React.Fragment>
                <Backdrop show={this.props.show} clicked={this.props.toggle} />
                <div className={classes.Modal}>
                    <div className={classes.ModalClose} onClick={this.props.toggle}><p>&times;</p></div>
                    {this.props.children}
                </div>
            </React.Fragment>
    , this.element);
    }   
}

export default Modal