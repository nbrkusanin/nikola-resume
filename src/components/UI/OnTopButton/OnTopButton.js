import React from 'react';

import classes from './OnTopButton.module.scss';

const OnTopButton = (props) => {
    return(
        <div onClick={props.onTop} className={classes.OnTopButton}>
            <i className="fa fa-angle-up"></i>
        </div>
    )
}

export default OnTopButton