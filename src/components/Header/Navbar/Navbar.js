import React from 'react';
import classes from './Navbar.module.scss';
import AnchorLink from 'react-anchor-link-smooth-scroll';

const Navbar = (props) => {

    const assignedClasses = [classes.Navbar];

    if(props.show) {
        assignedClasses.push(classes.ShowNav)
    }

    return (
        <React.Fragment>
            <div className={classes.HamBtnContainer}>
                <div className={classes.HamBtn} onClick={props.click}>
                    <div className={classes.Bar1}></div>
                    <div className={classes.Bar2}></div>
                    <div className={classes.Bar3}></div>
                </div>
            </div>
            <div className={assignedClasses.join(' ')}>
                
                <AnchorLink href="#AboutMe" className={classes.AnchorLink}>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    About Me
                </AnchorLink>
                <AnchorLink href="#WorkExperience" className={classes.AnchorLink}>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    Work Experience
                </AnchorLink>
                <AnchorLink href="#skills" className={classes.AnchorLink}>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    Key Skills
                </AnchorLink>
                <AnchorLink href="#education" className={classes.AnchorLink}>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    Education
                </AnchorLink>
                <AnchorLink href="#languages" className={classes.AnchorLink}>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    Languages
                </AnchorLink>
                <AnchorLink href="#personality" className={classes.AnchorLink}>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    Personality
                </AnchorLink>
                <AnchorLink href="#contact" className={classes.AnchorLink}>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    Contact
                </AnchorLink>
            </div>
        </React.Fragment>
    )
}

export default Navbar