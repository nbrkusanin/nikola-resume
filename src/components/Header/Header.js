import React from 'react';

import Navbar from './Navbar/Navbar';
import HeaderTitle from './HeaderTitle/HeaderTitle';
import classes from './Header.module.scss';

const Header = (props) => {
    return(
        <div className={classes.HeaderContainer}>
            <Navbar click={props.handleHamBtn} show={props.showNavbar}/>
            <HeaderTitle />
            <div className={classes.BottomLine}></div>
        </div>
    )
}

export default Header