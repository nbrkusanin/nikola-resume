import React from 'react';
import classes from './HeaderTitle.module.scss';
import AnchorLink from 'react-anchor-link-smooth-scroll';

const HeaderTitle = () => {
    return (
        <div className={classes.HeaderTitleContainer}>
            <div 
                style={{
                    position: 'absolute',
                    top: '0',
                    left: '0',
                    width: '100%',
                    height:  '100%'

                }}></div>
            <div className={classes.MouseIcon}>
                <div className={classes.Wheel}></div>
                <AnchorLink className={classes.Scroll} href='#AboutMe'></AnchorLink>
            </div>
            <div className={classes.Intro}>
                <div className={classes.IntroSub}>Nikola Brkusanin</div>
                <h1>Frontend <span>Developer</span></h1>
                <div className={classes.SocialIcons}>
                    <ul className={classes.ListInline}>
                        <li>
                            <a href="https://www.linkedin.com/in/nikola-brkusanin-76256a194/"><i className="fa fa-linkedin"></i></a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/nikola.brkusanin"><i className="fa fa-facebook"></i></a>
                        </li>
                        <li>
                            <a href="https://twitter.com/Nikola13851258"><i className="fa fa-twitter"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default HeaderTitle