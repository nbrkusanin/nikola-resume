import React from 'react';

import classes from './Languages.module.scss';

const Languages = () => {
    return(
        <div className={classes.Languages} id="languages">
            <div className={classes.LanguagesContainer}>
                <h2>Languages</h2>
                <div className={classes.LanguagesListCont}>
                    <ul className={classes.LanguagesList}>
                        <li className={classes.ListItem}>
                            <i className="fa fa-globe fa-3x"></i>
                            <div className={classes.ListItemData}>
                                <h3>SERBIAN</h3>
                                <p>Native language</p>
                            </div>
                            <i className="fa fa-globe fa-3x"></i>
                        </li>
                        <li className={`${classes.ListItem} ${classes.Diff}`}>
                            <i className="fa fa-globe fa-3x"></i>
                            <div className={classes.ListItemData}>
                                <h3>ENGLISH</h3>
                                <p>Advanced-medium level (B2)</p>
                            </div>
                            <i className="fa fa-globe fa-3x"></i>
                        </li>
                    </ul>
                </div>
            </div>
            <div className={classes.BottomLine}></div>
        </div>
    )
}

export default Languages